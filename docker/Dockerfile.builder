ARG GO_VERSION=1.19.3
FROM golang:$GO_VERSION as builder

RUN apt install make
