package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"github.com/opentracing/opentracing-go/log"
	"github.com/sirupsen/logrus"
	"gitlab.com/steveivy/goldfrog/pkg/blog"
)

var version string // set in linker with ldflags -X main.version=

var logger = logrus.New()

func main() {
	logger.SetLevel(logrus.DebugLevel)

	var postsDir string
	var dbFile string
	var verbose bool
	var dryRun bool
	var showVersion bool
	var startDateVal string = ""
	var endDateVal string = ""

	var startDate time.Time
	var endDate time.Time

	userHomeDir, _ := os.UserHomeDir()
	goldfrogHome, found := os.LookupEnv("BLOGHOME")
	if !found {
		goldfrogHome = filepath.Join(userHomeDir, "goldfrog")
	}

	flag.StringVar(
		&postsDir, "posts-dir",
		goldfrogHome+"/posts",
		"Location of your posts (Jekyll-compatible markdown)")

	flag.StringVar(
		&dbFile, "db",
		goldfrogHome+"/blog.db",
		"File path to sqlite db for indexed content")

	flag.StringVar(
		&startDateVal, "start-date",
		"",
		"Only persist posts on or after START-DATE")

	flag.StringVar(
		&endDateVal, "end-date",
		"",
		"Only persist posts on or before END-DATE")

	flag.BoolVar(
		&verbose, "v", false,
		"Enable verbose logging")

	flag.BoolVar(
		&showVersion, "version", false,
		"Print the version")

	flag.BoolVar(
		&dryRun, "dry-run", false,
		"Just parse options and exit")

	flag.Parse()

	logger.Debugf("startDateVal: %v", startDateVal)

	if startDateVal != "" {
		layout := "2006-01-02"
		startDateParsed, err := time.Parse(layout, startDateVal)
		if err != nil {
			logger.Fatal("Error while parsing date:", err)
			return
		}
		startDate = startDateParsed
		logger.Debug("Persisting posts from ", startDate.Format("2006-01-02"))
	}
	logger.Debugf("startDate: %v", startDate)

	logger.Debugf("endDateVal: %v", endDateVal)
	if endDateVal != "" {
		layout := "2006-01-02"
		endDateParsed, err := time.Parse(layout, endDateVal)
		if err != nil {
			logger.Fatal("Error while parsing date:", err)
			return
		}
		endDate = endDateParsed
		logger.Debug("Persisting posts before ", endDate.Format("2006-01-02"))
	}

	logger.Debugf("endDate: %v", endDate)

	if showVersion {
		fmt.Println(version)
		return
	}

	logger.Infof("Persisting posts in db: %s to: %s", dbFile, postsDir)
	repo := blog.FilePostsRepo{
		PostsDirectory: postsDir,
	}

	db, err := blog.GetDb(dbFile)
	if err != nil {
		log.Error(err)
		return
	}

	postOpts := blog.GetPostOpts{
		Limit: -1,
	}
	logger.Debug("startDate.IsZero? ", startDate.IsZero())
	if !startDate.IsZero() {
		postOpts.StartDate = startDate
	}
	logger.Debug("endDate.IsZero? ", endDate.IsZero())
	if !endDate.IsZero() {
		postOpts.EndDate = endDate
	}

	posts := blog.GetPosts(db, postOpts)

	if dryRun {
		logger.Infof("Found %d posts with options: %v", len(posts), postOpts)
		return
	}

	for _, post := range posts {
		err := repo.SavePostFile(post)
		if err != nil {
			log.Error(err)
			return
		}
	}
}
