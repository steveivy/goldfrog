module gitlab.com/steveivy/goldfrog

go 1.19

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
	github.com/dghubble/oauth1 v0.7.1
	github.com/fsnotify/fsnotify v1.6.0
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/gomarkdown/markdown v0.0.0-20221013030248-663e2500819c
	github.com/leekchan/gtf v0.0.0-20190214083521-5fba33c5b00b
	github.com/mattn/go-mastodon v0.0.6
	github.com/mattn/go-sqlite3 v1.14.17
	github.com/microcosm-cc/bluemonday v1.0.21
	github.com/opentracing/opentracing-go v1.2.0
	github.com/sirupsen/logrus v1.9.0
	github.com/sivy/go-twitter v0.0.0-20200228143626-89362039a5e4
	github.com/spf13/viper v1.14.0
	github.com/stretchr/testify v1.8.1
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dghubble/sling v1.4.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/magiconair/properties v1.8.6 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/pelletier/go-toml/v2 v2.0.5 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/afero v1.9.3 // indirect
	github.com/spf13/cast v1.5.0 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stretchr/objx v0.5.0 // indirect
	github.com/subosito/gotenv v1.4.1 // indirect
	golang.org/x/net v0.2.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
