SERVER_OUT := goldfrogd
INDEXER_OUT := indexer
PERSISTOR_OUT := persister
PKG := gitlab.com/steveivy/goldfrog

VERSION := $(shell git describe --always --abbrev=8)
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/)

_PHONY: server indexer persister

server:
	@echo "building goldfrog server ${VERSION}"
	go build -v -o ${SERVER_OUT} -ldflags="-X main.version=${VERSION}" cmd/goldfrogd/main.go

indexer:
	@echo "building goldfrog indxer ${VERSION}"
	go build -v -o ${INDEXER_OUT} -ldflags="-X main.version=${VERSION}" cmd/indexer/main.go

persister:
	@echo "building goldfrog persistor ${VERSION}"
	go build -v -o ${PERSISTOR_OUT} -ldflags="-X main.version=${VERSION}" cmd/persister/main.go

test:
	go test -short ${PKG_LIST}

_PHONY: all
all: server indexer persister

.PHONY: run
run: server
	./${SERVER_OUT}
